const assert = require("assert");
const pool = require("../dao/database");
const config = require("../dao/confiq");
let zipregex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

const logger = config.logger;


module.exports = {
  validatestudenthome(req, res, next) {
    console.log("validate studenthome");
    console.log(req.body);


    try {
      const { name, street, housenumber, postalcode, telnumber, city } = req.body;
      assert(typeof name === "string", "name is missing!");
      assert(typeof street === "string", "street is missing!");
      assert(typeof housenumber === "number", "housenumber is missing!");
      assert(typeof postalcode === "string", "postalcode is missing!");
      assert(typeof city === "string", "city is missing!");
      assert(typeof telnumber === "string", "telnumber is missing!");
      assert(zipregex.test(postalcode), "zipcode wrong format");
      assert(telnumber.length == 10, "phone number wrong format");
      console.log("studenthome data is valid");
      next();
    } catch (err) {
      console.log("studenthome data is invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },


//create
  create: (req, res, next) => {
    logger.info("studenthome.create called");
    const home = req.body;

    let { name, street, housenumber, postalcode, telnumber, city } = home;

    const userid = req.userId;

    let sqlQuery =
      "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?, ?, ?, ?, ?, ?, ?)";
    logger.debug("createHome", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("createHome", error);
        res.status(400).json({
          message: "createHome failed connection!",
          error: err,
        });
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [name, street, housenumber, userid, postalcode, telnumber, city],
          (error, results, fields) => {
            connection.release();
            if (error) {
              logger.error("createHome", error.toString());
              res.status(400).json({
                message: "home already exists",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("results: ", results);
              res.status(200).json({
                result: {
                  id: results.insertId,
                  ...home,
                },
              });
            }
          }
        );
      }
    });
  },

  getAll: (req, res, next) => {
    logger.info("studenthome.getAll called");
    console.log(req.query);
    let sqlQuery = "SELECT sh.ID, sh.Name, sh.Address, sh.House_Nr, sh.UserID, sh.Postal_Code, sh.Telephone, sh.City, user.First_Name, user.Last_Name, user.Email FROM studenthome AS sh INNER JOIN user ON sh.UserID = user.ID";
    const queryParams = Object.entries(req.query);

    if (queryParams.length > 0) {
      let queryString = queryParams
        .map((param) => {

          return `${param[0]} = '${param[1]}'`;
        })
        .reduce((a, b) => {

          return `${a} AND ${b}`;
        });
      logger.info("queryString:", queryString);
      sqlQuery += ` WHERE ${queryString};`;
    }

    logger.debug("getAll", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        console.log(err);
        res.status(400).json({
          message: "GetAll failed!",
          error: err.toString(),
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetAll failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            if (queryParams.length > 0 && mappedResults.length == 0) {
              res.status(404).json({
                error: "No entries were found",
              });
            } else {
              res.status(200).json({
                result: mappedResults,
              });
            }
          }
        });
      }
    });
  },
  getById: (req, res, next) => {
    logger.info("studenthome.info called");
    const homeId = req.params.homeId;
    let sqlQuery = "SELECT sh.ID, sh.Name, sh.Address, sh.House_Nr, sh.UserID, sh.Postal_Code, sh.Telephone, sh.City, user.First_Name, user.Last_Name, user.Email FROM studenthome AS sh INNER JOIN user ON sh.UserID = user.ID WHERE sh.ID = " + homeId;

    pool.getConnection(function (err, connection) {

      if (err) {
        res.status(400).json({
          message: "GetById failed!",
          error: err,
        });

      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            if (mappedResults.length == 0) {
              res.status(404).json({
                status: "No homes found with that ID",
              });
            } else {
              logger.info(mappedResults);
              res.status(200).json({
                result: mappedResults,
              });
            }
          }
        });
      }
    });
  },

  update: (req, res, next) => {
    const home = req.body;
    console.log("update.studenthome called");
    const userid = req.userId;
    const homeId = req.params.homeId;
    let { name, street, housenumber, postalcode, telnumber, city } = home;

    let sqlQuery =

      "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, UserID = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = " + homeId;
    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("updateStudentHome", error);
        res.status(400).json({
          message: "StudentHome failed getting connection!",
          error: err,
        });
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [name, street, housenumber, userid, postalcode, telnumber, city],
          (error, results, fields) => {
            connection.release();
            if (error) {
              logger.error("StudentHomeHome", error.toString());
              res.status(400).json({
                message: "StudentHome failed calling query",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("results: ", results);
              if (results.affectedRows === 0) {
                res.status(400).json({
                  error: "ID does not exist",
                });
              } else {
                res.status(200).json({
                  result: {
                    id: results.insertId,
                    ...home,
                  },
                });
              }
            }
          }
        );
      }
    });
  },

  delete: (req, res, next) => {
    console.log("delete.studenthomeId called");
    const homeId = req.params.homeId;
    const userid = req.userId;

    pool.getConnection(function (err, connection) {

      if (err) {
        res.status(400).json({
          message: "delete failed!",
          error: err,
        });
      }

      let sqlQuery = "DELETE FROM studenthome WHERE id = ? AND UserID = ?";

      connection.query(sqlQuery, [homeId, userid], (error, results, fields) => {
        connection.release();
        if (error) {
          res.status(400).json({
            message: "Could not delete item",
            error: error,
          });
        }
        if (results) {
          if (results.affectedRows === 0) {
            logger.trace("item was NOT deleted");
            res.status(404).json({
              error: "Item not found of you do not have access to this item",
            });
          } else {
            logger.trace("item was deleted");
            res.status(200).json({
              result: "successfully deleted item",
            });
          }
        }
      });
    });
  }
}