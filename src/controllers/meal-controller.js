const log = require("tracer").console();
const assert = require("assert");
const config = require("../dao/confiq");
const logger = config.logger;
const pool = require("../dao/database");

module.exports = {
  validateMeal(req, res, next) {
    logger.info("validate meal.");
    logger.info(req.body);

    try {
      const { name, description, available, price, allergies, ingredients } =
        req.body;
      assert(typeof name === "string", "name is missing!");
      assert(typeof description === "string", "description is missing!");
      assert(typeof available === "string", "date available is missing!");
      assert(typeof price === "number", "price is missing!");
      assert(typeof allergies === "string", "allergy information is missing!");
      assert(typeof ingredients === "string", "ingredients are missing!");
      console.log("Meal data is valid");
      next();
    } catch (err) {
      console.log("Meal data is not correct: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  create: (req, res, next) => {
    logger.info("create meal called");
    const meal = req.body;
    let { name,  description, available, price, allergies, ingredients, maxParticipants } = meal;

    const userid = req.userId;
    const homeid = req.params.homeId;

    var datenow = new Date().toLocaleString();

    var parsedDate = Date.parse(available);
    var dateavailable = new Date(parsedDate);
    logger.info(dateavailable);

    datenow = new Date().toISOString().slice(0, 19).replace("T", " ");

    logger.info(datenow);
    logger.info(dateavailable);

    let sqlQuery =
      "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `MaxParticipants`, `UserID`, `StudenthomeID`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    logger.debug("createMeal", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("createMeal:", err);
        res.status(400).json({
          message: "createMeal failed getting connection!",
          error: err,
        });
      }
      if (connection) {
       
        connection.query(
          sqlQuery,
          [
            name,
            description,
            ingredients,
            allergies,
            datenow,
            dateavailable,
            price,
            maxParticipants,
            userid,
            homeid,
          ],
          (error, results, fields) => {
       
            connection.release();
          
            if (error) {
              logger.error("create Meal", error.toString());
              res.status(400).json({
                message: "create Meal failed calling the query",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("results: ", results);
              res.status(200).json({
                result: {
                  id: results.insertId,
                  ...meal,
                },
              });
            }
          }
        );
      }
    });
  },



  
  update: (req, res, next) => {
    const meal = req.body;
    console.log("mealupdate called");
    const userid = req.userId;
    const mealid = req.params.mealId;

    

    const homeid = req.params.homeId;
    let { name,  description, available, price, allergies, ingredients, maxParticipants } = meal;

    var parsedDate = Date.parse(available);
    var dateavailable = new Date(parsedDate);

    let sqlQuery =

    "UPDATE meal SET Name = ?, Description = ?, Ingredients = ?, Allergies = ?, OfferedOn = ?, Price = ?, MaxParticipants = ? WHERE ID = ? AND StudentHomeID = ? AND UserID = ?";
    pool.getConnection(function (err, connection) {

      if (err) {

        logger.error("update the Meal", error);

        res.status(400).json({

          message: "Meal failed connection",

          error: err,

        });

      }

      if (connection) {

        connection.query(

          sqlQuery,

          [name,  description, ingredients, allergies, dateavailable, price, maxParticipants, mealid, homeid, userid,  ],

          (error, results, fields) => {

            connection.release();

            if (error) {

              logger.error("Meal: ", error.toString());

              res.status(400).json({

                message: "Meal failed calling the query!",

                error: error.toString(),

              });

            }

            if (results) {

              logger.trace("results: ", results);

              if (results.affectedRows === 0) {

                res.status(400).json({

                  error: "ID does not exist!",

                });

              } else {

                res.status(200).json({

                  result: {

                    id: results.insertId,

                    ...meal,

                  },

                });

              }

            }

          }

        );

      }

    });
  },


  getAll: (req, res, next) => {
    logger.info("meal.getAll called");
    const homeid = req.params.homeId;
    let sqlQuery = "SELECT meal.ID, meal.Name, meal.Description, meal.Ingredients, meal.Allergies, meal.CreatedOn, meal.OfferedOn, meal.Price, meal.UserID, meal.StudentHomeID, meal.MaxParticipants, user.First_Name, user.Last_Name, user.Email FROM meal INNER JOIN user ON meal.UserID = user.ID WHERE StudenthomeID =" + homeid;
    logger.debug("getAll", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "GetAll has failed!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetAll failed",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },

  info: (req, res, next) => {
    logger.info("meal.info called");
    const id = req.params.homeId;
    const mealid = req.params.mealId;

    logger.info(
      "Get aangeroepen op /api/studenthome/" + id + "/meal/" + mealid
    );
    

    let sqlQuery =
      "SELECT meal.ID, meal.Name, meal.Description, meal.Ingredients, meal.Allergies, meal.CreatedOn, meal.OfferedOn, meal.Price, meal.UserID, meal.StudentHomeID, meal.MaxParticipants, user.First_Name, user.Last_Name, user.Email FROM meal INNER JOIN user ON meal.UserID = user.ID WHERE StudenthomeID =" + id +" AND meal.ID = " + mealid;
    logger.debug("getById", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "GetById failed",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById has failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            if (mappedResults.length === 0) {
              res.status(404).json({
                error: "there are no meals with this ID ",
              });
            } else {
              res.status(200).json({
                result: mappedResults,
              });
            }
          }
        });
      }
    });
  },

  delete: (req, res, next) => {
    logger.info("meal.delete called");
    const homeid = req.params.homeId;
    const userid = req.userId;
    const mealid = req.params.mealId;
    logger.debug(
      "update",
      "mealid =",
      mealid,
      "homeid: ",
      homeid,
      "userid =",
      userid
    );

  
    pool.getConnection(function (err, connection) {
     
      if (err) {
        res.status(400).json({
          message: "delete has failed!",
          error: err,
        });
      }
   
      let sqlQuery =
        "DELETE FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?";
      connection.query(
        sqlQuery,
        [mealid, homeid, userid],
        (error, results, fields) => {
         
          connection.release();
          
          if (error) {
            res.status(400).json({
              message: "Could not delete item",
              error: error,
            });
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace("item was NOT deleted");
              res.status(401).json({
                result: {
                  error:
                    "Item not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("item was deleted");
              res.status(200).json({
                result: "successfully deleted item!",
              });
            }
          }
        }
      );
    });
  },
};
