const log = require("tracer").console();
const assert = require("assert");
const config = require("../dao/confiq");
const logger = config.logger;
const pool = require("../dao/database");

module.exports = {
  

  create: (req, res, next) => {
    logger.info("meal.create called");


    const userid = req.userId;
    const homeid = req.params.homeId;
    const mealid = req.params.mealId;

    var datenow = new Date().toLocaleString();

  


    datenow = new Date().toISOString().slice(0, 19).replace("T", " ");

    logger.info(datenow);


    let sqlQuery =
      "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, ?)";
    logger.debug("createMeal", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("signup: ", err);
        res.status(400).json({
          message: "failed connection!",
          error: err,
        });
      }
      if (connection) {
       
        connection.query(
          sqlQuery,
          [
            userid,
            homeid,
            mealid,
            datenow,
            
          ],
          (error, results, fields) => {
            
            connection.release();
             
            if (error) {
              logger.error("signup-", error.toString());
              res.status(400).json({
                message: "failed calling query",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("the results: ", results);
              res.status(200).json({
                result: {
                  
                  result: "signup is succesfull!",
                },
              });
            }
          }
        );
      }
    });
  },



  
 

  getAll: (req, res, next) => {
    logger.info("meal.getAll called");
    const mealid = req.params.mealId;
   
    let sqlQuery = "SELECT user.First_Name, user.Last_Name, user.Email, user.Student_Number FROM participants INNER JOIN user ON participants.UserID = user.ID WHERE MealID = "+ mealid
    logger.debug("getAll", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "GetAll has failed!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetAll failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace(" results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },
  getById: (req, res, next) => {
    logger.info("meal.getAll called");
    const mealid = req.params.mealId;
    const participantid = req.params.participantId;
   
    let sqlQuery = "SELECT user.First_Name, user.Last_Name, user.Email, user.Student_Number FROM participants INNER JOIN user ON participants.UserID = user.ID WHERE MealID = "+ mealid + " AND UserID = "+ participantid
    logger.debug("getAll", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "GetAll has failed please try again",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetAll not working",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },

 

  delete: (req, res, next) => {
    logger.info("meal.signoff called");
    const homeid = req.params.homeId;
    const userid = req.userId;
    const mealid = req.params.mealId;
    logger.debug(
      "update",
      "mealid =",
      mealid,
      "homeid: ",
      homeid,
      "userid =",
      userid
    );

    // get connection
    pool.getConnection(function (err, connection) {
      // if error
      if (err) {
        res.status(400).json({
          message: "signoff has failed!.",
          error: err,
        });
      }
      // Use the connection
      let sqlQuery =
        "DELETE FROM participants WHERE StudenthomeID = ? AND mealID = ? AND UserID = ?";
      connection.query(
        sqlQuery,
        [homeid, mealid,  userid],
        (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            res.status(400).json({
              message: "Could not do signoff",
              error: error,
            });
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace("signoff was NOT completed");
              res.status(401).json({
                result: {
                  error:
                    "signoff not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("signoff is successfull");
              res.status(200).json({
                result: "signoff is successfull!",
              });
            }
          }
        }
      );
    });
  },
};
