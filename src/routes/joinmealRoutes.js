const express = require("express");

const usercontroller = require("../controllers/user-controller");

const joinmealcontroller = require("../controllers/joinmeal-controller");

const { route } = require("./studenthome-route");

const router = express.Router();

router.post ("/studenthome/:homeId/meal/:mealId/signup", usercontroller.validateToken, joinmealcontroller.create);
router.put ("/studenthome/:homeId/meal/:mealId/signoff", usercontroller.validateToken, joinmealcontroller.delete);
router.get ("/meal/:mealId/participants", joinmealcontroller.getAll ); //alle gebruikers
router.get("/meal/:mealId/participants/:participantId", joinmealcontroller.getById); // 1 gebruiker

module.exports = router;