process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../myapp");
const assert = require("assert");
const jwt = require("jsonwebtoken");

chai.should();
chai.use(chaiHttp);

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-1 should return valid error when required value is not present", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/0")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            street: "spuistraat",
            housenumber: 1,
            postalcode: "2222BB",
            city: "Breda",
            telnumber: "0666666666",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            done();
          });
      });
    });
  });
});

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-2 should return valid error when zipcode is wrong format", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/0")
          .set("authorization", "Bearer " + token)
          .send({
            name: "herreshuis",
            street: "spuistraat",
            housenumber: 1,
            postalcode: "2222BBBB",
            city: "Breda",
            telnumber: "0666666666",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            done();
          });
      });
    });
  });
});

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-3 should return valid error when phone number is wrong format", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/0")
          .set("authorization", "Bearer " + token)
          .send({
            name: "herreshuis",
            street: "spuistraat",
            housenumber: 1,
            postalcode: "2222BB",
            city: "Breda",
            telnumber: "066666666677",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            done();
          });
      });
    });
  });
});

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-4 should return valid error when home doesn't exists", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/100")
          .set("authorization", "Bearer " + token)
          .send({
            name: "herreshuis",
            street: "spuistraat",
            housenumber: 1,
            postalcode: "2222BB",
            city: "Breda",
            telnumber: "0666666666",
          })
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });
});

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-5 should reject studenthome when not signed in", (done) => {
      token = "faketoken";
      chai
        .request(server)
        .put("/api/studenthome/0")
        .set("authorization", "Bearer " + token)
        .send({
          name: "herreshuis",
          street: "spuistraat",
          housenumber: 1,
          postalcode: "2222BB",
          city: "Breda",
          telnumber: "0666666666",
        })
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });
});

describe("StudentHome", function () {
  describe("update", function () {
    it("UC-204-6 should accept a valid studenthome", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/2")
          .set("authorization", "Bearer " + token)
          .send({
            name: "herreshuis",
            street: "spuistraat",
            housenumber: 1,
            postalcode: "2222BB",
            city: "Breda",
            telnumber: "0666666666",
          })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });
});
