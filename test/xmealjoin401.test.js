process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../myapp");
const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../src/dao/database");

chai.should();
chai.use(chaiHttp);



describe("JoinMeal", function () {
  describe("Signup", function () {
    it("UC-401-1 should Signup when cardvalue is present", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/2/meal/2/signup")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            

            done();
          });
      });
    });
  });
});
