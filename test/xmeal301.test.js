process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../myapp");
const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../src/dao/database");

chai.should();
chai.use(chaiHttp);


const INSERT_STUDENTHOME ="INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
  "(1, 'herreshuis', 'address', 1, 1, '1111AA', 066666666666, 'Breda' ) " 

describe("Meal", function () {
  describe("create", function () {
    it("UC-301-1 should return valid error when required value is not present", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/meal")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            descr: "fries",
            available: "12-01-2021",
            price: 1,
            allergies: "potato",
            ingredients: "potato",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be.a("string").that.equals("name is missing!");
            error.should.be.a("string");

            done();
          });
      });
    });
  });
});

describe("Meal", function () {
  describe("create", function () {
    it("UC-301-2 should return valid error when not signed in", (done) => {
      token = "Fake";
      chai
        .request(server)
        .post("/api/studenthome/1/meal")
        .set("authorization", "Bearer " + token)
        .send({
          name: "fries",
          descr: "fries",
          available: "12-01-2021",
          price: 1,
          allergies: "potato",
          ingredients: "potato",
          
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          done();
        });
    });
  });
});


  