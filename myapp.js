


const express = require('express');
const database = require("./src/dao/database");
const studenthomeroute = require("./src/routes/studenthome-route");
const mealRoutes = require("./src/routes/mealRoutes");
const userRoutes = require("./src/routes/userRoutes");
const joinmealRoutes = require("./src/routes/joinmealRoutes");
const port = process.env.PORT ||3000
 
const app = express();
app.use(express.json()); 
var logger = require('tracer').console();
 
logger.log("Database info: " + database.info)
logger.log(database.db)
 
app.all("*", (req, res, next) => {

  const reqMethod = req.method

  const reqUrl = req.originalUrl

  console.log("Request requested: "  + reqMethod + " " + reqUrl)

  next()

})
 
app.use("/api", studenthomeroute);
app.use("/api", userRoutes);
app.use("/api", mealRoutes);
app.use("/api", joinmealRoutes);
 
app.get("/api/info", (req, res) => {
  logger.log("Info endpoint called:")
  let result = {
    message: "Hello World!!",
    name: "Oscar Verhagen"
  }
  res.status(200).json(result)

})
 
 


// Catch-all route

app.all("*", (req, res, next) => {

  console.log("Catch-all endpoint aangeroepen");

  next({ message: "Endpoint '" + req.url + "' does not exist", errCode: 401 });

});



// Errorhandler

app.use((error, req, res, next) => {

  console.log("Errorhandler called! ", error);

  res.status(error.errCode).json({

    error: "Some error occurred",

    message: error.message,

  });

});
 
  app.listen(port, () => {
  logger.log(`Example app listening at port http://localhost:${port}`)
  });
 
  module.exports = app;